import React, {
  FC, useCallback, useEffect,
} from 'react';
import { useSwear } from '@swear-js/react';
import {
  Alert,
  Button, Card, List, Typography,
} from 'antd';
import { useHistory } from 'react-router';
import { filmsSwear } from '../swears/filmsSwear';
import { api } from '../api';

export const MainPage: FC = () => {
  const [{ data, loading }, { fetchFilms }] = useSwear(filmsSwear);
  const history = useHistory();

  useEffect(() => {
    fetchFilms();
  }, []);

  const remove = useCallback((id: string) => () => {
    api.delete(`/api/film/${id}`)
      .then(fetchFilms)
      .catch(() => alert('Something went wrong'));
  }, []);

  const create = useCallback(() => {
    history.push('/film/create');
  }, []);

  return loading ? <span>Loading...</span> : (
    <List
      header={(
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ fontWeight: 'bold' }}>Films</div>
          <Button type="primary" onClick={create} style={{ marginLeft: 'auto' }}>Create</Button>
        </div>
      )}
      bordered
      dataSource={data}
      renderItem={(item: any) => (
        <List.Item style={{ display: 'flex', alignItems: 'center' }}>
          <Alert message={item.ready ? 'Доступно' : 'Обрабатывается'} type={item.ready ? 'success' : 'warning'} />
          <p style={{ margin: 0, marginLeft: 20 }}>{item.title}</p>
          <Button style={{ marginLeft: 'auto' }} onClick={() => history.push(`/seances/${item._id}`)}>Seances</Button>
          <Button style={{ marginLeft: 'auto' }} onClick={remove(item._id)}>Remove</Button>
        </List.Item>
      )}
    />
  );
};
