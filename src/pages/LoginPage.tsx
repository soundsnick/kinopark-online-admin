import React, {
  FC, useCallback, useEffect, useState,
} from 'react';

import { Button, Input } from 'antd';
import { useHistory } from 'react-router';
import { api } from '../api';

export const LoginPage: FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem('token')) {
      history.push('/');
    }
  }, []);

  const login = useCallback(() => {
    setLoading(true);
    api.post('/api/auth/login', { email, password })
      .then((res) => {
        localStorage.setItem('token', res.data?.token);
        window.location.reload();
      })
      .catch(() => {
        alert('Incorrect credentials');
      })
      .finally(() => setLoading(false));
  }, [email, password]);

  return loading ? <span>Loading...</span> : (
    <>
      <Input value={email} onChange={(e) => setEmail(e.currentTarget.value)} placeholder="Email" />
      <Input style={{ marginTop: 5 }} value={password} onChange={(e) => setPassword(e.currentTarget.value)} placeholder="Password" type="password" />
      <Button style={{ marginTop: 15 }} type="primary" onClick={login}>Login</Button>
    </>
  );
};
