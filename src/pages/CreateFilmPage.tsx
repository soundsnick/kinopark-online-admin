import React, {
  ChangeEvent,
  ChangeEventHandler,
  FC, MutableRefObject, useCallback, useEffect, useMemo, useRef, useState,
} from 'react';

import { Button, Input } from 'antd';
import { useHistory } from 'react-router';
import { api, apiCdn } from '../api';

export const CreateFilmPage: FC = () => {
  const [title, setTitle] = useState('');
  const [director, setDirector] = useState('');
  const [description, setDescription] = useState('');
  const [image, setImage] = useState('');
  const [promoImage, setPromoImage] = useState('');
  const [duration, setDuration] = useState(0);
  const [files, setFiles] = useState<any>(null);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const fileRef = useRef();

  const data = useMemo(() => ({
    title,
    description,
    director,
    image,
    promoImage,
    duration,
  }), [title, description, director, image, promoImage, duration]);

  function dataURItoBlob(dataURI: any) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    const byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ab], { type: mimeString });
  }

  const upload = useCallback((id: string) => {
    const form = new FormData();

    if (files) {
      setLoading(true);
      form.append('file', dataURItoBlob(files));
      apiCdn.post(`/upload/${id}`, form, { headers: { 'content-type': 'multipart/form-data' } })
        .catch(async () => {
          await api.delete(`/api/film/${id}`);
        })
        .finally(() => {
          history.push('/');
          setLoading(false);
        });
    } else {
      api.delete(`/api/film/${id}`)
        .then(() => {
          alert('Try again');
        })
        .finally(() => setLoading(false));
    }
  }, [files]);

  const create = useCallback(() => {
    setLoading(true);
    api.post('/api/film', data)
      .then((res) => {
        upload(res.data._id);
      })
      .finally(() => setLoading(false));
  }, [data]);

  return loading ? <span>Loading...</span> : (
    <>
      <Input value={title} onChange={(e) => setTitle(e.currentTarget.value)} placeholder="Title" />
      <Input value={director} onChange={(e) => setDirector(e.currentTarget.value)} placeholder="Director" />
      <Input.TextArea value={description} onChange={(e) => setDescription(e.currentTarget.value)} placeholder="Description" />
      <Input value={image} onChange={(e) => setImage(e.currentTarget.value)} placeholder="Image URL" />
      <Input value={promoImage} onChange={(e) => setPromoImage(e.currentTarget.value)} placeholder="Promo Image URL" />
      <Input value={duration} onChange={(e) => setDuration(Number(e.currentTarget.value))} placeholder="Duration in seconds" />
      <Input
        type="file"
        onChange={(e) => {
          const reader = new FileReader();

          if (e.target.files?.[0]) {
            reader.readAsDataURL(e.target.files[0]);
          }
          reader.onload = (readerEvent) => {
            // @ts-ignore
            setFiles(readerEvent.target.result);
          };
        }}
        placeholder="File"
      />
      <Button disabled={files === null} style={{ marginTop: 15 }} type="primary" onClick={create}>Publish</Button>
    </>
  );
};
