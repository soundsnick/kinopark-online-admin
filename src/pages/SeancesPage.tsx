import React, {
  FC, useCallback, useEffect,
} from 'react';
import { useSwear } from '@swear-js/react';
import {
  Alert,
  Button, Card, List, Typography,
} from 'antd';
import { useHistory, useParams } from 'react-router';
import { filmsSwear } from '../swears/filmsSwear';
import { api } from '../api';
import { seancesSwear } from '../swears/seancesSwear';

export const SeancesPage: FC = () => {
  const [{ data, loading }, { fetchSeances }] = useSwear(seancesSwear);
  const history = useHistory();
  const params = useParams<{ id: string }>();

  useEffect(() => {
    fetchSeances(params.id);
  }, []);

  const remove = useCallback((id: string) => () => {
    api.delete(`/api/seance/${id}`)
      .then(() => fetchSeances(params.id))
      .catch(() => alert('Something went wrong'));
  }, []);

  const create = useCallback(() => {
    history.push(`/seances/${params.id}/create`);
  }, []);

  const formatDate = (date: string) => {
    const datetime = new Date(date);
    return `${datetime.getDay()}.${datetime.getMonth()}.${datetime.getFullYear()} - ${datetime.getHours()}:${datetime.getMinutes()}`;
  };

  return loading ? <span>Loading...</span> : (
    <List
      header={(
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <div style={{ fontWeight: 'bold' }}>Seances</div>
          <Button type="primary" onClick={create} style={{ marginLeft: 'auto' }}>Create</Button>
        </div>
      )}
      bordered
      dataSource={data}
      renderItem={(item: any) => (
        <List.Item style={{ display: 'flex', alignItems: 'center' }}>
          <p style={{ margin: 0 }}>{formatDate(item.datetime)}</p>
          <p style={{ margin: 0, marginLeft: 20 }}>
            {item.price}
            тг
          </p>
          <p style={{ margin: 0, marginLeft: 20 }}>
            Куплено:
            {item.users.length}
          </p>
          <Button style={{ marginLeft: 'auto' }} onClick={remove(item._id)}>Remove</Button>
        </List.Item>
      )}
    />
  );
};
