import React, {
  ChangeEvent,
  ChangeEventHandler,
  FC, MutableRefObject, useCallback, useEffect, useMemo, useRef, useState,
} from 'react';

import { Button, Input, Select } from 'antd';
import { useHistory, useParams } from 'react-router';
import { useSwear } from '@swear-js/react';
import { api, apiCdn } from '../api';
import { filmsSwear } from '../swears/filmsSwear';

export const CreateSeancePage: FC = () => {
  const params = useParams<{ id: string }>();
  const history = useHistory();

  const [{ data: films, loading: loadingFilms }, { fetchFilms }] = useSwear(filmsSwear);
  const [datetime, setDatetime] = useState('');
  const [price, setPrice] = useState(0);
  const [film, setFilm] = useState(params.id);
  const [loading, setLoading] = useState(false);

  const data = useMemo(() => ({
    datetime,
    price,
  }), [datetime, price]);

  useEffect(() => {
    fetchFilms();
  }, []);

  const create = useCallback(() => {
    setLoading(true);
    api.post(`/api/seance/create/${film}`, data)
      .then((res) => {
        history.push(`/seances/${film}`);
      })
      .finally(() => setLoading(false));
  }, [data]);

  return loading || loadingFilms ? <span>Loading...</span> : (
    <>
      <Input value={datetime} onChange={(e) => setDatetime(e.currentTarget.value)} placeholder="Datetime" type="datetime-local" />
      <Input value={price} onChange={(e) => setPrice(Number(e.currentTarget.value))} placeholder="Price" type="number" />
      <Select value={film} onChange={(newValue) => setFilm(newValue)}>
        {films.map((n: any) => (
          <Select.Option value={n._id}>{n.title}</Select.Option>
        ))}
      </Select>
      <Button style={{ marginTop: 15 }} type="primary" onClick={create}>Publish</Button>
    </>
  );
};
