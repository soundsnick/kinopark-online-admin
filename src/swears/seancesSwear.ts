import { createSwear } from '@swear-js/react';
import { api } from '../api';

const defaultState = {
  data: [],
  loading: false,
};

export const seancesSwear = createSwear('seances', defaultState, (mutate) => ({
  fetchSeances: (id: string) => {
    mutate({ ...defaultState, loading: true });
    api.get(`/api/film/${id}/seances`)
      .then((res) => {
        mutate({ data: res.data, loading: false }, 'Finished fetching');
      })
      .catch(() => {
        mutate({ ...defaultState });
      });
  },
}));
