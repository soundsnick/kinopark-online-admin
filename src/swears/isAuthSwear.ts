import { createSwear } from '@swear-js/react';

export const isAuthSwear = createSwear('isAuth', localStorage.getItem('token'), () => null);
