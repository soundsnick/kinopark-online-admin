import { createSwear } from '@swear-js/react';
import { api } from '../api';

const defaultState = {
  data: [],
  loading: false,
};

export const filmsSwear = createSwear('films', defaultState, (mutate) => ({
  fetchFilms: () => {
    mutate({ ...defaultState, loading: true });
    api.get('/api/films')
      .then((res) => {
        mutate({ data: res.data, loading: false }, 'Finished fetching');
      })
      .catch(() => {
        mutate({ ...defaultState });
      });
  },
}));
