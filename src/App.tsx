import React, { useEffect } from 'react';
import { createStore } from '@swear-js/core';
import { swearContext } from '@swear-js/react';
import { swearLogger } from '@swear-js/logger';
import { Route, Switch } from 'react-router-dom';
import { useHistory } from 'react-router';
import { MainPage } from './pages/MainPage';
import { Navigation } from './components/Navigation';
import 'antd/dist/antd.css';
import { LoginPage } from './pages/LoginPage';
import { CreateFilmPage } from './pages/CreateFilmPage';
import { SeancesPage } from './pages/SeancesPage';
import { CreateSeancePage } from './pages/CreateSeancePage';

function App() {
  const store = createStore({ onPatch: swearLogger() });
  const history = useHistory();

  useEffect(() => {
    if (!localStorage.getItem('token')) {
      history.push('/login');
    }
  }, []);
  return (
    <swearContext.Provider value={store}>
      <div style={{
        maxWidth: 500, margin: 'auto', fontFamily: 'sans-serif', lineHeight: 1.2,
      }}
      >
        <Navigation />
        <Switch>
          <Route path="/login">
            <LoginPage />
          </Route>
          <Route path="/film/create">
            <CreateFilmPage />
          </Route>
          <Route path="/seances/:id/create">
            <CreateSeancePage />
          </Route>
          <Route path="/seances/:id">
            <SeancesPage />
          </Route>

          <Route path="/">
            <MainPage />
          </Route>
        </Switch>
      </div>
    </swearContext.Provider>
  );
}

export default App;
