import React, { FC } from 'react';
import { useSwear } from '@swear-js/react';
import { Link } from './Link';
import { isAuthSwear } from '../swears/isAuthSwear';

export const Navigation: FC = () => {
  const [isAuth] = useSwear(isAuthSwear);
  return isAuth ? (
    <div style={{
      display: 'flex', alignItems: 'center', justifyContent: 'space-between', borderBottom: '1px solid #d7d7d7', padding: '20px 0', marginTop: 20, marginBottom: 20,
    }}
    >
      <h2>Kinopark Admin</h2>
      <div style={{ display: 'flex', alignItems: 'center', gap: 15 }}>
        <Link to="/">Films</Link>
      </div>
    </div>
  ) : (
    <span style={{
      display: 'block', padding: '20px 0', paddingTop: 150, color: 'red',
    }}
    >
      Sign in to access
    </span>
  );
};
