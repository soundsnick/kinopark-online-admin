import axios from 'axios';

export const api = axios.create({
  baseURL: 'http://192.168.1.236:3000',
  headers: localStorage.getItem('token') ? {
    Authorization: `Bearer ${localStorage.getItem('token')}`,
  } : {},
});

export const apiCdn = axios.create({
  baseURL: 'http://192.168.1.236:3002',
  headers: {
    Authorization: 'Bearer adkaspdap23123123asda',
  },
});
